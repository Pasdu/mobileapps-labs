package csd203.michael.walsh.whackamole.enemy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

import csd203.michael.walsh.whackamole.R;
import csd203.michael.walsh.whackamole.WhackaView;

public class Greame extends Enemy {

    public Greame()
    {
        Init();

    }

    @Override
    protected void Init()
    {
        SetSprites(Enemy.GREAME_REGULAR);
        SetHP(1);
        SetImage(GetSpriteSet().GetIdleSprite());
        SetAgility(1300);
        SetSize(175);
        SetStrength(20);
        super.Init();
        MoverLoop();
    }

    private void MoverLoop()
    {

        if(!alive)
            return;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if(alive)
                {
                    Move();
                    MoverLoop();
                }

            }
        }, GetAgility());
    }

    @Override
    protected void UpdateCount(int val)
    {
        SetCount(EnemyTypes.Greames, val);
    }


}