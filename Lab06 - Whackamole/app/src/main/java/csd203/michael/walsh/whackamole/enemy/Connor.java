package csd203.michael.walsh.whackamole.enemy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

import csd203.michael.walsh.whackamole.R;
import csd203.michael.walsh.whackamole.WhackaView;

public class Connor extends Enemy {

    public Connor( )
    {
        Init();

    }

    @Override
    protected void Init()
    {
        SetSprites(Enemy.CONNOR_REGULAR);
        SetHP(3);
        SetImage(GetSpriteSet().GetIdleSprite());
        SetAgility( 2000);
        SetSize(225);
        SetStrength(35);
        super.Init();

    }

    @Override
    protected void UpdateCount(int val)
    {
        SetCount(EnemyTypes.Connors, val);
    }




}