package csd203.michael.walsh.whackamole;

public class SpriteSet {
    private int IdleSprite;
    private int AttackSprite;
    private int HitSprite;
    private int DeadSprite;

    public SpriteSet(int idle, int attack, int hit, int dead)
    {
        IdleSprite = idle;
        AttackSprite = attack;
        HitSprite = hit;
        DeadSprite = dead;
    }

    public int GetIdleSprite(){return IdleSprite;}
    public int GetAttackSprite() {return AttackSprite;}
    public int GetHitSprite() {return HitSprite;}
    public int GetDeadSprite(){return DeadSprite;}
}
