package csd203.michael.walsh.whackamole;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.text.Layout;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

import csd203.michael.walsh.whackamole.enemy.Connor;
import csd203.michael.walsh.whackamole.enemy.Enemy;
import csd203.michael.walsh.whackamole.enemy.Greame;
import csd203.michael.walsh.whackamole.enemy.Mike;
import csd203.michael.walsh.whackamole.enemy.Steves;

public class WhackaView extends SurfaceView implements Runnable{
    private static CopyOnWriteArrayList<Enemy> Enemies = new CopyOnWriteArrayList<>();

    public static int score = 0;

    private static int X_MAX;
    private static int Y_MAX;

    final public static int MAX_HP = 1000;

    private boolean alive;

    private boolean playing = true;

    private SurfaceHolder mSurfaceHolder;

    private static int hp = 1000;
    public static int GetHP(){return hp;}
    private Paint imagePainter;

    public MainActivity activity;

    TimerTask nextSpawn;

    public WhackaView( Context context )
    {
        super(context);
        imagePainter = new Paint();

        activity = ((MainActivity) context);
        Init();
        mSurfaceHolder = getHolder();

    }

    public void Init()
    {
        new Thread(this).start();
    }

    public void EnrageMikes( boolean state )
    {
        Mike mike = null;
        for (Enemy e: Enemies) {
            try{
                mike = ((Mike) e);
            }
            catch(Exception f)
            {
                continue;
            }
            finally {
                if(mike != null)
                    mike.Enrage(state);
            }

        }
    }

    public void Hit(int damage)
    {
        if(!alive)
            return;

        hp -= damage;
        MainActivity.instance.UpdateHP();

        if(hp < 0)
        {
            GameOver();
        }
        else if(hp > MAX_HP)
            hp = MAX_HP;
    }

    public void SetMaxCoords(int x, int y)
    {
        X_MAX = x;
        Y_MAX = y;

        NewGame();
    }

    public void KillEnemy(final Enemy e)
    {
        final Enemy enemy = e;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Enemies.remove(enemy);
                MainActivity.instance.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        invalidate();
                    }
                });
            }
        }, 2000);

        MainActivity.instance.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                invalidate();
            }
        });
    }

    @Override
    public void run()
    {
        while(playing)
        {
            if(mSurfaceHolder.getSurface().isValid())
            {
                Canvas canvas = mSurfaceHolder.lockCanvas();

                canvas.drawBitmap(BitmapFactory.decodeResource( getResources(), R.drawable.classr),null,new Rect(0,0,X_MAX, Y_MAX),imagePainter);
                for(Enemy e : Enemies)
                {
                    canvas.drawBitmap(e.GetImage(),null, e.GetRect(), imagePainter );
                }

                mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        }


    }

    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        float x = e.getX();
        float y = e.getY();

        switch (e.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                for (Enemy enemy : Enemies)
                {
                    if(enemy.GetRect().contains((int)x, (int)y))
                    {
                        if(enemy.isAlive())
                            enemy.hit();
                    }
                }
                break;
            default:
                break;
        }

        return false;
    }

    static public int[] GenerateRandomCoordinates()
    {
        Random r = new Random();

        int xCoord;
        int yCoord;

        boolean overlapped = false;

            xCoord =  r.nextInt(X_MAX - 200);
            yCoord =  r.nextInt(Y_MAX - 200);

        int[] coords = new int[2];
        coords[0] = xCoord;
        coords[1] = yCoord;

        return coords;
    }

    public void GenerateEnemy( Enemy.EnemyTypes enemyType, int[] coords )
    {
        if(hp < 0)
            return;
        Enemy newEnemy;
        switch(enemyType)
        {
            case Connors:
                newEnemy = new Connor();
                break;
            case Steves:
                newEnemy = new Steves();
                break;
            case Greames:
                newEnemy = new Greame();
                break;
            default:
                newEnemy = new Mike();
                break;
        }

        newEnemy.SetPos(coords[0], coords[1]);
        Enemies.add(newEnemy);

    }

    public void EnemySpawnEvent()
    {
        if(!alive)
            return;
        //Spawn enemies on a regular interval
        //the interval decreases over time
        GenerateEnemy(Enemy.EnemyTypes.Steves, GenerateRandomCoordinates());
        GenerateEnemy(Enemy.EnemyTypes.Connors, GenerateRandomCoordinates());
        GenerateEnemy(Enemy.EnemyTypes.Mikes, GenerateRandomCoordinates());
        GenerateEnemy(Enemy.EnemyTypes.Greames, GenerateRandomCoordinates());

        //Spawn more enemies per interval
        //as the player frags more


        new Timer().schedule(nextSpawn = new TimerTask() {
            @Override
            public void run() {
                if(!alive)
                    return;
                EnemySpawnEvent();
                MainActivity.instance.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        invalidate();
                    }
                });
            }
        }, 5000);

    }

    public boolean isAlive()
    {
        return alive;
    }

    public void NewGame()
    {
        //Cleanup old game
        CleanupGame();
        //Setup new game

        //Start new game
        EnemySpawnEvent();
    }

    public void GameOver()
    {
        alive = false;
        nextSpawn.cancel();
        for(Enemy e : Enemies)
        {
            e.Kill();
        }
        activity.GameOver();
    }

    public void CleanupGame()
    {

        Enemies.clear();
        MainActivity.instance.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                invalidate();
            }
        });
        System.gc();
        score = 0;
        hp = MAX_HP;
        alive = true;
        Enemy.ClearCounts();
    }

}
