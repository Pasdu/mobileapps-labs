package saltcallage.com.pang;

import android.graphics.Point;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

public class MainActivity extends AppCompatActivity {
    private PangView v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        v = new PangView(this);
        setContentView(v);

    }



    public Point GetBounds()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels - 155;
        int width = displayMetrics.widthPixels;
        return new Point(width, height);
    }
}
