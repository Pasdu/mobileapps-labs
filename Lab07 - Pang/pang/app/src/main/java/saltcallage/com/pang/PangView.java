package saltcallage.com.pang;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class PangView extends SurfaceView implements Runnable{
    private float paddleX = 0;
    private float paddleWidth = 150;

    private CopyOnWriteArrayList<BreakBlock> blockField = new CopyOnWriteArrayList<>();

    private int ballVelocityScale = 10;
    private Point ballVelocity;
    private Point ballPosition;
    private float ballScale = 55;


    private boolean playing = true;
    SurfaceHolder holder;


    private Paint paddlePaint;
    private Paint ballPaint;
    private Paint[] blockPaint;

    private MainActivity main;

    private Point screenSpace;


    private Rect paddle;
    private Rect ball;


    public PangView(Context context)
    {
        super(context);

        paddlePaint = new Paint();
        ballPaint = new Paint();
        blockPaint = new Paint[5];
        blockPaint[0] = new Paint();
        blockPaint[0].setColor(Color.RED);
        blockPaint[1] = new Paint();
        blockPaint[1].setColor(Color.BLUE);
        blockPaint[2] = new Paint();
        blockPaint[2].setColor(Color.GREEN);
        blockPaint[3] = new Paint();
        blockPaint[3].setColor(Color.MAGENTA);
        blockPaint[4] = new Paint();
        blockPaint[4].setColor(Color.YELLOW);



        main = ((MainActivity) context);

        screenSpace = new Point(main.GetBounds());
        ballPosition = new Point(0,0);
        ballVelocity = new Point(0,0);



        SetupGame();

        holder = this.getHolder();
    }

    private void SetupGame()
    {
        ballPosition.x = screenSpace.x/2;
        ballPosition.y = screenSpace.y/3;

        ballVelocity.x = 0;
        ballVelocity.y = 0;

        Timer timer = new Timer();

        GenerateBlockfield();

        timer.schedule(new TimerTask() {
            public void run() {
                StartMatch();
            }
        }, 2000);

        new Thread(this).start();
    }

    private void StartMatch()
    {
        ballVelocity.x = 0;
        ballVelocity.y = -15;
    }

    @Override
    public void run()
    {

        while(playing)
        {

            Canvas canvas;
            if( holder.getSurface().isValid() )
            {
                canvas = this.getHolder().lockCanvas();
                canvas.save();
                canvas.drawColor(Color.WHITE);
                //Draw The Paddle
                canvas.drawRect(paddle = GetPaddleRectangle(), (paddlePaint));
                //Draw The Ball
                canvas.drawRect( ball = GetBallRectangle(), ballPaint);
                // Draw the breakers
                boolean foundCollision = false;
                for(BreakBlock block : blockField)
                {
                    if(CheckForBallCollision(block.GetRect()) && !foundCollision)
                    {

                        ballVelocity.x = (ballPosition.x - block.getCoords().x);
                        ballVelocity.y = (ballPosition.y - block.getCoords().y);

                        ballVelocity = CapVelocity(ballVelocity);
                        foundCollision = true;
                        blockField.remove(block);
                    }
                    canvas.drawRect( block.GetRect() , blockPaint[block.GetPaint() ]);
                }
                canvas.restore();
                this.getHolder().unlockCanvasAndPost(canvas);

            }

            CheckForPaddleCollision();
        }
    }

    private Point CapVelocity(Point velocity)
    {
        double Differential = (ballVelocityScale * 1.3) / Math.hypot(velocity.x, velocity.y);
        return new Point((int)(velocity.x * Differential),(int)( velocity.y * Differential));
    }

    private boolean CheckForBallCollision(Rect rec)
    {
        if(rec.contains(ball.left, ball.top))
            return true;
        if(rec.contains(ball.right, ball.top))
            return true;
        if(rec.contains(ball.left, ball.bottom ))
            return true;
        if(rec.contains(ball.right, ball.bottom))
            return true;

        return false;
    }

    private Rect GetBallRectangle()
    {
        Point maxes = main.GetBounds();

        int upperBound;
        int lowerBound;
        int leftBound;
        int rightBound;

        if( ballPosition.x - ballScale/2 < 0 )
        {
            leftBound = 0;
            rightBound = (int)ballScale;
            ballPosition.x = (int)(ballScale/2 + 1);
            if( ballVelocity.x < 0 )
                ballVelocity.x *= -1;
                ballVelocity.y -= 1.2f;

        }
        else if( ballPosition.x + ballScale / 2 > maxes.x)
        {
            leftBound = (int)(maxes.x - ballScale);
            rightBound = (maxes.x);
            ballPosition.x = (int)(maxes.x - ballScale/2);
            if( ballVelocity.x > 0)
            {
                ballVelocity.x *= -1;
                ballVelocity.y -= 1.2f;
            }

        }
        else
        {
            leftBound = (int)(ballPosition.x - ballScale/2);
            rightBound = (int)(leftBound + ballScale);
        }

        if( ballPosition.y - ballScale/2 < 0 )
        {
            upperBound = 0;
            lowerBound = (int)ballScale;
            LosePoint();
        }
        else if( ballPosition.y + ballScale/2 > maxes.y)
        {
            upperBound = (int)(maxes.y - ballScale);
            lowerBound = maxes.y;
            ballPosition.y = (int)(maxes.y - ballScale/2 - 1);
            if(ballVelocity.y > 0)
                ballVelocity.y *= -1;
        }
        else
        {
            upperBound = (int)(ballPosition.y - ballScale/2);
            lowerBound = (int)(ballPosition.y + ballScale/2);
        }

        return new Rect(leftBound, upperBound, rightBound, lowerBound);

    }


    private void LosePoint()
    {
        SetupGame();
    }

    private Rect GetPaddleRectangle()
    {
        Point maxes = main.GetBounds();

        int upperBound = 25;
        int lowerBound = 65;
        int leftBound;
        int rightBound;

        if( paddleX <  paddleWidth / 2)
        {
            leftBound = (0);
            rightBound = (int)(leftBound + paddleWidth);
        }
        else if( paddleX + paddleWidth/2 >  maxes.x)
        {
            rightBound = (maxes.x);
            leftBound = (int)(rightBound - paddleWidth);
        }
        else
        {
            leftBound = (int)(paddleX - paddleWidth/2);
            rightBound = (int)(paddleX + paddleWidth/2);
        }

        return new Rect(leftBound, upperBound, rightBound, lowerBound);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction())
        {
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_DOWN:
                //Set Pang Paddle position to x
                paddleX = x;
                break;
        }
        return true;
    }

    public void GenerateBlockfield()
    {
        blockField.clear();

        Point cursor = new Point(BreakBlock.width + BreakBlock.padding, screenSpace.y /2);

        while(cursor.y < screenSpace.y)
        {
            while(cursor.x < screenSpace.x - BreakBlock.width)
            {
                Random rng = new Random();
                Point coords = new Point(cursor.x, cursor.y);
                blockField.add(new BreakBlock(coords, rng.nextInt(5)));
                cursor.x += BreakBlock.width + BreakBlock.padding;
            }
            cursor.y += BreakBlock.height + BreakBlock.padding;
            cursor.x =  BreakBlock.width + BreakBlock.padding;
        }

    }

    public void CheckForPaddleCollision()
    {
        if(paddle == null)
            return;

        boolean foundCollision = false;
        if(paddle.contains(ball.left, ball.top) )
        {
            foundCollision = true;
            if (ballVelocity.y < 0)
            {
                ballVelocity.x += 10;
            }
        }

        if(paddle.contains(ball.right, ball.top))
        {
            foundCollision = true;
            {
                ballVelocity.x -= 10;
            }

        }
        if(foundCollision)
        {
            ballVelocity.y = 15;

            ballPosition.y = (int)(67 + ballScale/2);
        }

        ballPosition.x += ballVelocity.x;
        ballPosition.y += ballVelocity.y;
    }

}
