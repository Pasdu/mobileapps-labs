package saltcallage.com.pang;

import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

public class BreakBlock {
    private Point coords;

    static int width = 125;
    static int height = 55;
    static int padding = 25;

    private int paint;

    private Rect rect;

    public BreakBlock(Point c, int p)
    {
        coords = c;
        rect = new Rect(coords.x - width/2, coords.y - height/2, coords.x + width/2, coords.y + height/2);
        paint = p;
    }

    public Point getCoords()
    {
        return coords;
    }

    public int GetPaint(){
        return paint;
    }

    public Rect GetRect()
    {
        return rect;
    }


}
