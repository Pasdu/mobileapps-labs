package csd203.michael.walsh.texttospeechenhanced;

import android.app.Activity;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.URI;

public class AddAutoResponder extends AppCompatActivity {
    public final static String EXTRA_NEW_PHONE = "csd203.michael.walsh.texttospeech.enhanced.NEW_PHONE";
    public final static String EXTRA_NEW_RESPONSE = "csd203.michael.walsh.texttospeech.enhanced.NEW_RESPONSE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(csd203.michael.walsh.texttospeech.R.layout.activity_add_auto_responder);

        Button submitBtn = (findViewById(csd203.michael.walsh.texttospeech.R.id.submitbtn));

        final EditText phoneField = findViewById(csd203.michael.walsh.texttospeech.R.id.phoneText);
        final EditText messageField = findViewById(csd203.michael.walsh.texttospeech.R.id.messageText);

        submitBtn.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v)
            {

                Intent returnIntent = new Intent();
                returnIntent.putExtra(EXTRA_NEW_PHONE, phoneField.getText().toString());
                returnIntent.putExtra(EXTRA_NEW_RESPONSE, messageField.getText().toString());
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }
}
