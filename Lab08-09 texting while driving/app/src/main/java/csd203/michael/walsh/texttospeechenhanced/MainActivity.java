package csd203.michael.walsh.texttospeechenhanced;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    public static MainActivity instance;
    public TTSInitter tts;
    private String responseMessage = "I am driving right now, thank you.";
    private int MY_PERMISSIONS_REQUEST_SMS_RECEIVE = 10;

    private HashMap<String, String> autoResponses = new HashMap<>();

    private int ADD_CONTACT = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String[] requiredPermissions = new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.SEND_SMS, Manifest.permission.READ_EXTERNAL_STORAGE};

        for(String permis : requiredPermissions)
        {
            if (checkCallingOrSelfPermission(permis) != PackageManager.PERMISSION_GRANTED)
                ActivityCompat.requestPermissions(this, requiredPermissions, );
        }




        super.onCreate(savedInstanceState);
        setContentView(csd203.michael.walsh.texttospeech.R.layout.activity_main);
        tts = new TTSInitter(this);
        instance = this;
        Button setMsgBtn = findViewById(csd203.michael.walsh.texttospeech.R.id.changeMessage);
        setMsgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetResponse( ((EditText) findViewById(csd203.michael.walsh.texttospeech.R.id.messageText)).getText().toString() );
            }
        });

        Button newResponderBtn = findViewById(csd203.michael.walsh.texttospeech.R.id.addBtn);
        newResponderBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent responderIntent = new Intent(v.getContext(), AddAutoResponder.class);
                startActivityForResult( responderIntent, ADD_CONTACT);
            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK)
        {
            System.out.println(resultCode);
            Bundle results = data.getExtras();

            System.out.println(data.getStringExtra(AddAutoResponder.EXTRA_NEW_PHONE));

            AddNewAutoResponder(
                    data.getStringExtra(AddAutoResponder.EXTRA_NEW_PHONE),
                        data.getStringExtra(AddAutoResponder.EXTRA_NEW_RESPONSE));
        }

    }

    public void AddNewAutoResponder(String phone, String msg)
    {
        autoResponses.put(phone, msg);
        Button newResponseButton = new Button(this);
        LinearLayout responderList = findViewById(csd203.michael.walsh.texttospeech.R.id.autoresponders);
        String buttonString = phone + " " + msg;
        newResponseButton.setText(buttonString);
        responderList.addView(newResponseButton);
    }

    public void ReceivedMessage(final String smsMessageStr, final String smsAddress) {
        ((TextView) findViewById(csd203.michael.walsh.texttospeech.R.id.messageDisplay)).setText(smsMessageStr);
        System.out.println(smsAddress);
        if(((CheckBox) findViewById(csd203.michael.walsh.texttospeech.R.id.ttsEnabled)).isChecked())
            tts.speak(smsMessageStr, true);
        if(((CheckBox) findViewById(csd203.michael.walsh.texttospeech.R.id.autoResponse)).isChecked())
            SendResponse(smsMessageStr, smsAddress);
    }

    public void SetResponse(String msg)
    {
        responseMessage = msg;
        ((TextView) findViewById(csd203.michael.walsh.texttospeech.R.id.autoResponseDisplay)).setText(msg);
    }

    public void SendResponse(String dest,  String message)
    {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(dest, null, message, null, null);
    }

}
